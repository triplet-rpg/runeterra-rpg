# Заклинания III круга
![helmet-5.png](../img/helmets/helmet-5.png "Заклинания III круга")

<!-- Spell: Elementary Control -->
## Владение стихией
*Необходимо выбрать конкретную стихию*

Маг способен создавать, контролировать и/или развеивать выбранный тип стихии.
Количество создаваемой стихии, ровно как и качество её контроля,
определяется уровнем этого заклинания.

#### Примеры
 - **Пиромант:** Зажигает ветку в руке, тушит или зажигает факела, входя в помещение
 - **Жрец:** Глаза жреца начинают испускать сияние, освещает комнату, посылая в неё сферу света
 - **Некромант:** Концентрируясь на мертвой плоти, заставляет её <<ожить>>

#### Эффект
*Тип заклинания: Особое*

> **Это важно:**
> С помощью заклинания <<Владение стихией>> можно эмулировать эффекты
> других стихийных заклинаний I-II кругов магии
> (например, [<<Вреда>>][вред], [<<Преграды>>][преграда] или [<<Пут>>][путы]),
> если это контекстно допустимо, не выбивается из сеттинга и сути стихии,
> согласовано с логикой и не вызывает неодобрения Мастера.
> Уровень <<эмуляции>> эквивалентен уровню этого заклинания.
> В частности, это позволяет использовать выбранную стихию
> для нанесения прямого урона в боевых условиях (см. [<<Вред>>][вред]).

В случае, если <<Владение стихией>> эмулирует эффект другого заклинания,
используйте его правила для определения типа, продолжительности и других эффектов.
В противном случае то, что определяет магический бросок, определяется индивидуально,
а уровни заклинания будут следующими:
 - **Уровень 1 -- <<Касание>>:**  
   Заклинатель способен создавать *небольшие* количества стихии в своих руках
   (масса и объём значительно меньше его собственных),
   манипулировать стихией, касаясь её, заставлять её принимать и менять форму.
   Стихия теряет форму или исчезает после потери физического контакта с магом.
 - **Уровень 2 -- <<Взгляд>>:**  
   Заклинатель способен создавать *разумные* количества стихии в зоне прямой видимости,
   (масса и объём не могут превышать его собственные),
   манипулировать стихией взглядом, заставлять её принимать и менять форму.
   Стихия теряет форму или исчезает, если маг перестанет на неё смотреть.
 - **Уровень 3 -- <<Все в зоне действия>>:**  
   Заклинатель способен создавать *огромные* количества стихии в зоне прямой видимости
   (масса и объём мага, умноженные на показатель Шкалы),
   что позволяет взаимодействовать сразу с несколькими целями.
   Количество целей определяется показателем Шкалы.
   Стихия более не теряет форму и не исчезает, если заклинатель теряет контакт с ней.

---
<!-- Spell: Creature Protection -->
## Защита от существ
*Необходимо выбрать конкретный тип существ*

Маг создаёт мистическую защиту для кого-либо
или чего-либо от определённого типа существ,
и они будут стараться избегать причинения вреда,
а в случае такого будут получать урон сами.

#### Примеры
 - **Волшебник:** Защищает свой кошель от воров. Монеты в нём раскаляются, когда из берут без разрешения владельца
 - **Жрец:** Защищает себя от нежити, нежить упокаивается в его присутствии
 - **Друид:** Защищает священное дерево от аберраций. При приближении их атакует окружающая растительность

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]; или [Реакция], [Мгновенное]*

При изучении заклинания нужно выбрать тип существ, от которых оно защищает:
 - Если это *сверхъестественные* создания
   (в т.ч. те, у которых есть особенность <<Сверхъестественный>> / <<Неуязвимый>>),
   то нужно выбрать конкретный тип, например:
   феи, исчадия, небожители, пришельцы, нежить
 - Если это *обычные* создания, нужно указать категорию, например:
   змеи, волки, инсектоиды, аберрации
 - Если это *гуманоиды*, то необходимо уточнить, например:
   полурослики, воры, убийцы, дети (слишком общие характеристики исключаются)

Пока эффект заклинания активен,
каждый раз, когда такое существо агрессивно взаимодействует с подзащитным,
оно получает единицу урона за каждую ступень успеха
своих действий в отношении цели.
Т.е., если по подзащитному данное существо нанесло 3 раны,
то и существо получит 3 раны.
Аналогичный эффект будет и при применении существом других
атакующих и/или агрессивных способностей и умений.

<<Защита от существ>> может быть использована либо *активно*, либо *реактивно* в качестве защитной реакции:
 - В случае **активного** использования дарует цели эффект защиты на время,
   определяемое результатом магического броска.
   Допустимые цели заклинания увеличиваются с уровнем
 - В случае **реактивного** использования дарует магу эффект защиты на одно действие.
   Для успешного применения необходимо совершить магический бросок против числа эффективных успехов цели.
   Эффект не улучшается с уровнем

Уровни заклинания:
 - **Уровень 1 -- <<Ты ко мне не прикоснёшься>>:**  
   Маг может защитить только себя или свой личный предмет
   и только от действий, требующих непосредственного контакта
 - **Уровень 2 -- <<Взгляд>>:**  
   Маг может защитить кого-либо или что-либо
   движимое от любых действий в отношении него
 - **Уровень 3 -- <<Присутствие>>:**  
   Маг может защитить несколько объектов.
   Количество подзащитных определяется показателем Шкалы

Кроме того, за каждый последующий уровень заклинания
вы можете дополнительно выбрать тип существ,
от которого будете способны защищаться и защищать.
В случае использования [<<Прикрепления масти>>][прикрепить-масть]
вы можете выбрать дополнительно по типу за каждый уровень способности.

---
<!-- Spell: Change Size -->
## Изменение размера
*Необходимо выбрать увеличение или уменьшение*

Чародей изменяет размер существа или предмета,
делая его значительно больше, либо же -- наоборот -- значительно меньше.

#### Примеры
 - **Великан:** Становиться ещё больше
 - **Трикстер:** Меняет свои размеры для собственных нужд
 - **Гном:** Уменьшается

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

> **Это важно:**
> В случае изменения размера существа все его вещи, кроме личной вещи,
> находящейся при персонаже, не изменяют размер.
> Не снятые заранее предметы могут при этом испортиться.
> Доспех может нанести урон.
> Личная вещь, воспринимаемая существом,
> как крайне важная для него, также увеличивается/уменьшается.
> Неподготовленную цель вводит в замешательство.

Размер цели меняется в выбранном заранее направлении во столько раз,
каков показатель Шкалы мага.

Кроме того, имеется дополнительный эффект в зависимости от <<стороны>> изменения размера
 - **Увеличение** добавляет показатель Шкалы заклинателя ко всем броскам на проверки на *Запугивание*, *Местность*, *Телосложение* и *Защита*.
   Кроме того, увеличенная цель получает эффект статуса [Большой] или [Гигантский] (в зависимости от размера)
 - **Уменьшение** добавляет показатель Шкалы заклинателя ко всем броскам на проверки на *Скрытность*, *Медицина*, *Проницательность* и *Ловкость*.
   Кроме того, увеличенная цель получает эффект статуса [Мелкий]

Время действия определяется результатом броска.
Объект с уже изменённым размером примет обычные размеры,
если применить это заклинание повторно.

Уровни заклинания:
 - **Уровень 1 -- <<Гулливер>>:**  
   Колдун можете изменить собственный размер
 - **Уровень 2 -- <<Ты стал выше в моих глазах>>:**  
   Колдун может изменить размер другого персонажа
 - **Уровень 3 -- <<Атака титанов>>:**  
   Колдун может изменить размер нескольких целей в пределах своего восприятия,
   количество целей определяется показателем Шкалы

---
<!-- Spell: True Vision -->
## Истинное восприятие
Волшебник создаёт эффект, который нивелирует все прочие эффекты,
влияющие на восприятие цели, позволяя ей видеть сквозь завесы и сумраки ночи.

#### Примеры
 - **Шаман:** Заглядывает сквозь завесу и видит истину через мир духов
 - **Повелитель Зверей:** Усиливает свои чувства до максимально возможных в животном мире,
   инстинкты позволяют ему интерпретировать полученную информацию
 - **Чернокнижник:** Зрит через тени

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Считается, что цель начинает видеть, слышать,
ощущать во всех возможных спектрах, диапазонах
и чувствах в отношении объекта наблюдения,
воспринимать сверхъестественную ауру и следы на ней.
Помимо этого, позволяет видеть в темноте.

Время действия определяется результатом броска.

Уровни заклинания:
 - **Уровень 1 -- <<Я ощущаю>>:**  
   Заклинатель сам обретает истинное восприятие, но лишь на расстоянии вытянутой руки
 - **Уровень 2 -- <<Узри!>>:**  
   Заклинатель может даровать истинное восприятие кому либо помимо себя,
   радиус восприятия соответствует нормальной зоне восприятия цели воздействия
 - **Уровень 3 -- <<Взирайте>>:**  
   Заклинатель может даровать истинное восприятие нескольким целям,
   количество целей определяется показателем Шкалы

---
<!-- Spell: Pocket Dimension -->
## Карманное измерение
Чародей открывает небольшой портал в карманное измерение, где может хранить вещи.

#### Примеры
 - **Друид:** Складывает своё добро в дупло волшебного дерева.
 - **Геомант:** Прячет свои драгоценности в пещеру в недрах земли.
 - **Гидромант:** Погружает вещи на дно морское.

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

При успешном применении заклинатель создаёт
портал в карманное измерение небольшого размера
(не более одной сцены/локации),
размер самого портала зависит от уровня заклинания.
В портал можно беспрепятственно складывать любые вещи подходящего размера
и беспрепятственно же их оттуда доставать.
Пока заклинание *не* активно, карманное измерение,
ровно как и всё находящееся внутри, пребывает в стазисе.

Время жизни портала определяется результатом броска.

Уровни заклинания:
 - **Уровень 1 -- <<Нащупал>>:**  
   Чародей открывает портал *небольшого* размера,
   через который могут проходить карманные предметы --
   кошелёк, небольшие подручные инструменты, фрукты итп.
   Портал стационарен в пространстве
 - **Уровень 2 -- <<Вижу>>:**  
   Чародей открывает портал *среднего* размера,
   через который могут проходить предметы, которые можно держать одной рукой --
   одноручные мечи и другое оружие, походное снаряжение, домашние животные итп. 
   Портал может быть привязан к какому либо предмету,
   который можно переносить с собой, имеющему подходящую по размеру полость
 - **Уровень 3 -- <<Что тут у нас?>>:**  
   Чародей открывает портал *большого* размера,
   через который могут свободно проходить крупные предметы --
   двуручное оружие, башенные щиты, походная палатка, небольшой пленник итп.
   Портал открывается в любом удобном месте и может свободно перемещаться в пространстве

---
<!-- Spell: Greater Healing -->
## Мощное исцеление
*Необходимо выбрать конкретный тип существ, например:
люди, животные, кремниевые формы жизни, синтетики, роботы*

Усиление [<<Слабого лечения>>][слабое-лечение]

Волшебник способен исцелять страдающих,
буквально за считанные мгновения залечивая смертельные раны.

#### Примеры
 - **Тауматург:** Останавливает кровотечение
 - **Жрец:** Исцеляет силой своего бога
 - **Алхимик:** Использует зелье лечения

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Каждый успех лечит одну ранее полученную рану.
На критический успех исцеление восстанавливает
утраченные части тел и лечит [увечья] (убирает <<кресты>>).

Такое лечение может стабилизировать находящегося при смерти персонажа,
но не в состоянии мгновенно вывести его из комы.

> **Медленное лечение:**
> В случае [Долгой][долгое] активации это заклинание
> воздействует на всех союзников неподалёку,
> независимо от уровня заклинания.
> На третьем уровне такое исцеление полностью восстанавливает все раны,
> независимо от числа успехов.

Уровни заклинания:
 - **Уровень 1 -- <<Возложение рук>>:**  
   Целитель может лечить себя или союзника на дистианции касания
 - **Уровень 2 -- <<Исцеляющий взор>>:**  
   Целитель может лечить другую цель в области восприятия
 - **Уровень 3 -- <<Крик>>:**  
   Маг может исцелять несколько целей в области восприятия.
   Количество допустимых целей определяется показателем Шкалы

---
<!-- Spell: Metamorphosis -->
## Оборотничество
*Необходимо выбрать форму, в которую перевоплощается оборотень*

Оборотень способен принимать формы виданных (и невиданных) животных и не только.

#### Базовые формы
 - **Водоплав:**
    * Примеры: тюлени, дельфины, рыбы
    * Не может захлебнуться
    * Пребывая в воде, добавляет показатель Шкалы
      ко всем броскам на физические проверки
    * В воде перемещается со скоростью [<<Галопа>>][галоп]
 - **Быстроног:**
    * Примеры: газели, антилопы, олени
    * Бдительный, описание обстановки не тратит действия в бою
      (свойство {+бонусной+} *Внимательности*)
    * Перемещается со скоростью [<<Галопа>>][галоп]
    * Добавляет показатель Шкалы ко всем броскам на *Ловкость*
 - **Тяжелоступ:**
    * Примеры: быки, гориллы, летний медведь
    * Получает броню, равную показателю Шкалы
    * Добавляет показатель Шкалы ко всем броскам на *Телосложение*
    * Штраф: Перемещается с обычной скоростью (не <<Галопом>>)
 - **Небокрылый:**
    * Примеры: птицы, нетопыри, птерозавры
    * Умеет летать (в воздухе перемещается с обычной скоростью)
    * Штраф: *Не получает* дополнительного преимущества ни к каким физическим броскам
 - **Когтеклык:**
    * Примеры: волки, кугуары, рапторы
    * Хороший боец, прицельные атаки наносят урон
      (свойство {+бонусного+} *Нападения*)
    * Перемещается со скоростью [<<Галопа>>][галоп]
    * Добавляет показатель Шкалы ко всем броскам на *Нападение*
 - **Мелкорост:**
    * Примеры: грызуны, домашние кошки
    * Юркий, способен залезать в труднодоступные места 
    * Способен маскироваться даже тогда, когда противник его видит
    * Добавляет показатель Шкалы ко всем броскам на *Защиту*
 - **Склизкополз:**
    * Примеры: змеи, скорпионы, пауки
    * Способен ползать по вертикальным поверхностям
    * Добавляет показатель Шкалы к броскам на *Скрытность*
    * Штраф: Перемещается с обычной скоростью (не <<Галопом>>)

При желании можно придумать и описать другие формы для перевоплощения.
Так, например, вестайя может научиться превращаться в человека.

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

При перевоплощении оборотень получает дополнительные
описанные выше бонусы в зависимости от выбранной формы.
Кроме того, некоторые формы содержат дополнительные штрафы.
Например, большинство форм не являются разумными и магическими,
что не позволяет говорить и колдовать в таком состоянии.
Запрет колдовства не распространяется на это заклинание,
что позволяет свободно отменять перевоплощение
либо же менять форму с одной на другую прямо во время боя.

Бонусы к атрибутам даваемые этим заклинанием, улучшают общий результат,
а не добавляются к броску в виде преимущества или {+бонуса+}.

Время действия перевоплощения определяется результатом магического броска. 

Уровни заклинания:
 - **Уровень 1:**  
    * *Форма <<Волк>>:*  
      Доступно только базовое перевоплощение в существо эквивалентного размера.
      В таком состоянии невозможно колдовать или говорить
      (кроме как с другими животными)
 - **Уровень 2:**  
    * *Форма <<Альфа>>:*  
      Перевоплощение в *могучее* существо, явно отличающееся по размеру и массе.
      В таком состоянии всё ещё невозможно колдовать или говорить
      (кроме как с другими животными).
      Кроме того, даёт бонус в виде показателя Шкалы
      на все проверки *Социальных* Атрибутов против животных
    * *Форма <<Сфинкс>>:*  
      Позволяет перевоплотиться в гибрид двух видов,
      получающий преимущества и недостатки обоих видов,
      или в разумное магическое существо, получающее все преимущества перевоплощения
      и способное говорить и колдовать
 - **Уровень 3:**  
    * *Форма <<Мастодонт>>:*  
      Перевоплощение в *невероятно могучее* существо (например, динозавра),
      или в *невероятно маленькое* существо (например, муху или морскую свинку).
      Эффект эквивалентен тому, как если бы на нормальное перевоплощение наложили [<<Изменение размера>>][изменение-размера]
    * *Форма <<Химера>>:*  
      Позволяет перевоплотиться в разумное магическое существо,
      включающее в себя формы всех классов, получающее все их уникальные бонусы
      (и не получая никаких штрафов), включая бонусы к характеристикам

Кроме того, каждый уровень способности позволяет выбрать
новую доступную для перевоплощения форму.
Старые формы при этом (при желании) улучшаются.
В случае использования [<<Прикрепления масти>>][прикрепить-масть]
вы можете выбрать дополнительно по форме за каждый уровень способности.

---
<!-- spell:brilliance -->
## Озарение
Маг способен одарить интеллектом и навыками даже самых неспособных учеников.

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Перед активацией заклинания выбирается навык, кроме тех,
которыми цель владеет безупречно (имеет {+бонусный+} эффект).
Пока заклинание активно, цель теряет негативный *пассивный* эффект имеющегося у неё {-штрафного-} навыка,
или получает {+бонусный+} *пассивный* эффект имеющегося у неё нейтрального навыка.
<<Активные>> эффекты (гарантированный минимум и усиление критических неудач) остаются неизменными.

Уровни заклинания:
 - **Уровень 1 -- <<Самовнушение>>:**  
   Маг получает озарение сам
 - **Уровень 2 -- <<Менторство>>:**  
   Маг способен даровать озарение одной цели в пределах восприятия.
   Поддерживать контакт не требуется
 - **Уровень 3 -- <<Просвещение>>:**  
   Маг способен даровать озарение сразу нескольким целям
   (все цели получают одинаковое озарение,
   даже если это не имеет эффекта для отдельных из них).
   Количество целей определяется показателем Шкалы

---
<!-- Spell: Elementary Consumption -->
## Поглощение стихии
*Необходимо выбрать конкретную **элементарную** стихию и конкретный тип восполняемого ресурса*

Получая урон от выбранного типа стихи,
чародей восстанавливает ресурсы организма или личности,
будь то временную силу воли, варп, ману или душевные грузы.

#### Примеры
 - **Фанатик неугасимого пламени:** Снимает с себя чувство вины (душ.грузы), подвергая себя испытанию огнём
 - **Электромант:** Напитывает себя мощью, купаясь в грозе
 - **Шиноби:** Обретает мудрость и уверенность, перерабатывая своим телом яды

#### Эффект
*Тип: [Реакция], [Мгновенное]*

Это заклинание может быть активировано только
рефлекторно в качестве реакции на воздействие от выбранной стихии.
Заклинатель тратит соответствующий магический ресурс.

После того, как урон он стихии получен,
заклинатель восстанавливает пропорциональное количество выбранного ресурса --
по одной единице ресурса за каждый пункт полученного урона за каждый уровень заклинания.
(Например, получив три урона от огня на втором уровне заклинания, можно восстановить 6 ед. ресурса.)
В случае если тем или иным образом урон был нивелирован,
то эффект заклинания не срабатывает.

Магический бросок не требуется.
Уровень заклинания определяет число восстанавливаемого ресурса организма/личности за единицу полученного урона.

---
<!-- Spell: Search -->
## Поиск
*Необходимо выбрать спектр объектов, которые заклинатель способен искать*

Колдун может определять местоположение нужных ему объектов,
субъектов, предметов, персонажей или локаций.

#### Примеры
 - **Повелитель Зверей:** Посылает на поиск зверей
 - **Шаман:** Посылает на поиск духов
 - **Телепат:** Чувствует след сознания в коллективном бессознательном

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Заклинатель может определить направление и расстояние до объекта.
Как правило, необходимо иметь хоть какой то <<след>> искомого,
отпечаток ноги, вещь, к которой прикасалась цель поиска,
знать полное имя или побывать тесном контакте с целью,
заглянув в глаза или прикоснувшись ранее.
Чем <<след>> более личный, тем точнее и чище сработает эффект.

Результат магического броска определяет радиус и точность поиска.

Уровни заклинания:
 - **Уровень 1 -- <<Взять след>>:**  
   Необходимо иметь хоть какой то <<след>> искомого.
   В случае успеха колдун ощущает искомое только сам
 - **Уровень 2 -- <<Ищейка>>:**  
   Необходимо иметь хоть какой то <<след>> искомого.
   Колдун ощущает искомое сам, либо может передать его кому-либо в зоне восприятия --
   необходимо уточнить во время прочтения заклинания
 - **Уровень 3:**
    * *Эффект <<Навигатор>>:*  
      Колдун можете даровать чувство цели целой группе,
      количество определяется показателем Шкалы.
      Сам при этом ощущение искомого он не теряет
    * *Эффект <<Знание>>:*  
      Колдун способен искать практически без <<следа>>,
      но не может передать такое ощущение искомого

---
<!-- Spell: Spirit Projection -->
## Потусторонняя проекция
*Если космология подразумевает множество потусторонних слоёв реальности,
необходимо выбрать конкретный: мир мёртвых, варп, ад, небеса*

Волшебник может исторгнуть из тела сознание.
Персонаж впадает в бессознательное состояние,
но его душа оказывается рядом на границе реального и потустороннего миров.

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Персонаж может действовать, как обычно,
но он становиться невидимым для наблюдателей
обоих миров, если они не используют сверхъестественное
воздействие [<<Истинное восприятие>>](#истинное-восприятие).
Сам персонаж-проекция может наблюдать только пограничные явления.

Время действия заклинания определяется успешностью броска.

> **Пример:**
> Персонаж на границе с миром мёртвых может наблюдать
> только явления распада из материального мира,
> смертельно больных или раненных со стороны
> материального мира и души недавно умерших в мире мёртвых.

Уровни заклинания:
 - **Уровень 1 -- <<Я>>:**  
   Заклинатель может создать только собственную проекцию
   либо копию личного предмета, которого касается
 - **Уровень 2 -- <<Мимолётный взгляд>>:**  
   Маг может создать проекцию другого существа или предмета, если видит его
 - **Уровень 3:**  
    * *Эффект <<Знание>>:*  
      Маг может создать проекцию другого, хорошо знакомого существа или предмета,
      даже если он за пределами видимости
    * *Эффект <<Группа>>:*  
      Маг может создать несколько проекций объектов в зоне непосредственного восприятия.
      Количество определяется показателем Шкалы

---
<!-- Spell: Order -->
## Приказ
*Необходимо выбрать существ, на которые действует <<Приказ>>, например:
птицы, звери, гады, люди, небожители, феи, исчадия, иные*

Заклинатель отдаёт приказ одним словом, который цель выполняет не задумываясь.

#### Примеры
 - **Друид:** Высвобождает нужные ему инстинкты в жертве
 - **Тауматург:** Овладевает кровью в части тела жертвы
 - **Демонолог:** Вселяет в жертву злого духа

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Для успешного применения необходимо совершить магический бросок против сопротивления цели.
В случае успеха цель не задумываясь выполняет одно односложно определённое действие
(например, <<убей его>>, <<прочь>> или <<разденься>>).

Цели не обязательно знать язык.
Цель не будет выполнять самоубийственный приказ.

Уровни заклинания:
 - **Уровень 1 -- <<Касание>>:**  
   Только одна цель за раз.
   Колдун должен быть либо непосредственно перед целью, либо касаться её
 - **Уровень 2 -- <<Услышь мой голос>>:**  
   Колдун может отдать приказ на расстоянии слышимости/видимости
 - **Уровень 3 -- <<Внемлите!>>:**  
   Колдун может отдать один и тот же приказ нескольким целям, которые его воспринимают.
   Количество подчинённых определяется показателем Шкалы

<style type="text/css">
.col-md-9 hr { background-image: url('../img/splitters/splitter-5.png'); }
</style>

<!-- Общие магические термины -->
[быстрое]: ./spells.md#быстрое
[временное]: ./spells.md#временное
[долгое]: ./spells.md#долгое
[мгновенное]: ./spells.md#мгновенное
[перманентное]: ./spells.md#перманентное
[реакция]: ./spells.md#реакция

<!-- Прочие термины -->
[большой]: ../game-mechanics/special-rules.md#большой
[гигантский]: ../game-mechanics/special-rules.md#гигантский
[мелкий]: ../game-mechanics/special-rules.md#мелкий
[прикрепить-масть]: ./spells.md#прикрепить-масть
[увечья]: ../game-mechanics/special-rules.md#увечья

<!-- Заклинания -->
[вред]: ./spells-tier-2.md#вред
[галоп]: ./spells-tier-2.md#галоп
[изменение-размера]: ./spells-tier-3.md#изменение-размера
[преграда]: ./spells-tier-2.md#преграда
[путы]: ./spells-tier-1.md#путы
[слабое-лечение]: ./spells-tier-1.md#слабое-лечение
