<!-- spells.md -->
# Заклинания
## Стихии
Многие заклинания в игре требуют отсылаются в своём тексте к так называемым *Стихиям*.
Стихия -- это какая-либо природа сверхъестественного воздействия, например, огонь, вода или разум.
Все стихии относятся либо к *Элементарным* (имеющим непосредственное воплощение в этом мире),
либо к *Неэлементарным* (эфемерным сущностям, не имеющим конкретных формы, ~~цвета и запаха~~)

> **Это важно:**
> Не всякой стихии доступны все виды заклинаний,
> в этом отношении решение принимает Мастер.

### Элементарные стихии
 - **Взвесь:** Дымные, туманные, песчаные маги
 - **Вода:** Гидроманты, друиды, целители
 - **Воздух:** Аэроманты, шаманы, шиноби
 - **Земля:** Геоманты, шаманы, друиды
 - **Металл:** Геоманты, кузнецы, алхимики
 - **Молния:** Электроманты, жрецы, паладины
 - **Огонь:** Пироманты, паладины, демонологи
 - **Холод:** Криоманты, некроманты, шаманы
 - **Энтропия:** Некроманты, чернокнижники, демонологи

### Неэлементарные стихии
 - **Звук:** Барды, аэроманты, варвар
 - **Иллюзии:** Шиноби, трикстеры, барды
 - **Кровь:** Тауматурги, некроманты, демонологи
 - **Плоть:** Друиды, мутаторы, чернокнижники
 - **Разум:** Телепат, барды, прорицатели
 - **Свет:** Иерофанты, жрецы, астрономы
 - **Токсик:** Друиды, некроманты, шиноби
 - **Тьма:** Некроманты, чернокнижники, шиноби
 - **Флора:** Друиды, геоманты, шаманы

---
## Виды заклинаний
Все заклинания в системе относятся делятся на различные типы по двум разным категориям:
 - По типу действующего эффекта:
    * Мгновенное -- заклинание, имеющее разовый эффект
    * Временное -- заклинание, эффект которого существует некоторое время
      (если не сказано иного, то длительность измеряется в раундах)
    * Перманентное -- заклинание, эффект которого будет существовать до тех пор, пока не будет развеян
 - По типу сотворения (<<активации>>) заклинания:
    * Быстрое -- заклинание активируется практически мгновенно,
      зачастую это занимает один боевой раунд
    * Долгое -- заклинание активируется с помощью колдовского ритуала,
      длящегося от получаса до нескольких дней
    * Реакция -- заклинание активируется реактивно после триггера,
      описанного непосредственно в эффекте заклинания, и никак иначе.
      Такая активация не тратит действие в бою

Заклинания могут относится сразу к нескольким категориям,
в таком случае игрок сам выбирает подходящую.
Если в типе заклинания написано <<Быстрое или Долгое>>, то это означает,
что заклинание может быть активировано как <<по щелчку пальцев>>,
так и с помощью подходящего колдовского ритуала.
В таком случае эффект *<<Долгого>>* применения будет усилен:
 - Для *Временных* эффектов оно  увеличит время действия заклинания на одну степень
   (раунды станут часами, часы -- днями, дни -- месяцами, ...)
 - Для *Мгновенных* и *Перманентных* эффектов оно будет усилено иначе,
   на усмотрение игрового Мастера
   (например, снизит сложность, удвоит количество успехов или увеличит радиус действия)

### Мгновенное
Мгновенное заклинание -- это заклинание, имеющее разовый эффект.
Это может быть как непосредственный эффект (оглушение, нанесение урона),
так и что-то более абстрактное.

### Временное
Временное заклинание -- это заклинание, эффект которого существует некоторое время.
Если не сказано иного, то длительность измеряется в раундах.

#### Повторное применение
Если не сказано иного, то повторное применение *Временного* заклинания
снимает предыдущий эффект -- старая каменная преграда рассыплется, если призвать новую,
а с недавно подчинённого врага спадёт проклятие, если попытаться заполучить новую марионетку.
Неудача на повторном применении тоже развеивает предыдущий успех.

#### Обновление
Если же заклинатель хочет продлить действие эффекта
и цель является валидной с точки зрения активации заклинания,
то обновление заклинания допустимо без магического броска,
исключительно за трату магического ресурса --
число эффективных успехов будет таким же, как и в прошлый раз.
Критические успехи обновить подобным образом нельзя, но зачастую и не нужно.

### Перманентное
Перманентное заклинание -- это заклинание,
эффект которого будет существовать до тех пор, пока не будет развеян.

Обычно к перманентным эффектам относятся заклинания высших кругов магии.

### Быстрое
Быстрое заклинание активируется практически мгновенно,
зачастую это занимает один боевой раунд.

Быстрая магия зачастую применяется в стрессовых, нестандартных и боевых ситуациях.

### Долгое
Долгое заклинание активируется с помощью колдовского ритуала,
длящегося от получаса до нескольких дней.

Долгая магия, как правило, обладает особенно сильными эффектами,
что отражает всплеск сконцентрированной магической энергии.

### Реакция
Реакция заклинание активируется реактивно после триггера,
описанного непосредственно в эффекте заклинания, и никак иначе.
Такая активация не тратит действие в бою.

К реакциям обычно относится защитная магия.

---
## Круги магии
Все заклинания в системе разбиты на так называемые круги магии, или же круги заклинаний.
Заклинания более высокого круга, как правило, сильнее магии предыдущих кругов.

## Уровень заклинания
У каждого заклинания есть его уровень, обычно измеряемый от 1 до 3.
Чем выше уровень заклинания, тем оно сильнее.

В *большинстве* ситуаций эффект уровня заклинания стандартизирован,
однако для игрокам всё же следует читать, что даёт тот или иной уровень.

Эффекты уровней по умолчанию:
 - **Уровень 1:**  
   Применение только на себя (для зачарований)
   или на дальности касания (для проклятий)
 - **Уровень 2:**  
   Применение на одну цель в зоне восприятия
 - **Уровень 3:**  
   Применение на несколько целей в зоне восприятия

## Изучение магии
Все персонажи начинают игру с одним заклинанием первого круга первого уровня.
При получении нового уровня персонажа игроки могут вместо улучшения атрибутов
либо увеличить уровень одного из имеющихся у них заклинаний на 1,
либо изучить новое заклинание, либо получить новую специализацию.

### Покупка новых заклинаний
Из-за того, что неопытным магам высокоранговые заклинания неподвластны,
на изучение (<<покупку>>) новых заклинаний наложены следующие ограничения:
1. Нужно иметь не меньше изученных заклинаний, чем круг нового заклинания минус один
   (для изучения заклинания пятого круга нужно знать четыре других заклинания)
2. Нужно иметь показатель Шкалы не меньший, чем круг нового заклинания

Новое заклинание записывается в книгу заклинаний персонажа и,
если не сказано иного, имеет первый уровень.

> **Это важно:**
> Такие же требования соблюдаются и для применения соответствующего заклинания.
> В частности, если показатель Шкалы понизится во время игры,
> то герой может временно <<разучиться>> применять некоторые свои заклинания.
> Как только персонаж восстановит свой показатель Шкалы,
> он вернёт возможность активации таких заклинаний.

### Усиление заклинаний
Некоторые высокоранговые заклинания обозначены как *Усиления* других,
например [<<Перекачка сил>>][перекачка-сил] является усилением [<<Вреда>>][вред].

Если герою известно заклинание, *являющееся* усилением другого,
то он может применять все его <<ослабленные>> версии
(то есть все заклинания, усилением которых оно является -- рекурсивно),
используя уровень и специализации усиления.

Если же герою, наоборот, известно заклинание, *имеющее* усиление,
то вместо покупки самого усиления (если выполнены требования)
герой может *заменить* более слабое более сильным,
сохраняя его уровень и специализации.
А может и не заменять, получая новые специализации и уровень.

Стоит отметить, что такая замена учитывается
как покупка заклинания в рамках подсчёта числа изученных заклинаний,
что может обеспечивать выполнение требований для изучения/применения магии.
Это стоит отображать в листе персонажа любым понятным образом.

### Специализации
Многие заклинания требуют выбрать некоторую специализацию --
некоторый атрибут, магическую стихию или тип существ, с которыми можно взаимодействовать --
это всё обозначается *Специализацией* и указывается в скобках справа от заклинания, например:
<<[Защита от существ][защита-от-существ] (волки)>> или <<[Неуязвимость] (физический)>>.

Специализацию необходимо выбрать в момент изучения заклинания.
Некоторые заклинания позволяют выбрать дополнительные специализации при повышения их уровня.

#### <<Прикрепить масть>>
Если возникает необходимость получения дополнительной специализации,
герой может вместо повышения уровня заклинания <<прикрепить к нему масть>> --
новую специализацию, которая дополняет имеющиеся.
Таким образом можно получить на заклинании *все* возможные специализации
(что особенно актуально для заклинаний со всего двумя альтернативами).

Термин пошёл из других ролевых систем,
где к заклинаниям действительно прикреплялась карточная масть.

<!-- Заклинания -->
[вред]: ../magic/spells-tier-2.md#вред
[защита-от-существ]: ../magic/spells-tier-3.md#защита-от-существ
[неуязвимость]: ../magic/spells-tier-5.md#неуязвимость
[перекачка-сил]: ../magic/spells-tier-4.md#перекачка-сил
