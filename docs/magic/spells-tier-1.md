# Заклинания I круга
![helmet-3.png](../img/helmets/helmet-3.png "Заклинания I круга")

<!-- Spell: Inspire -->
## Воодушевление
*Похожего эффекта можно добиться, удачно совершив проверку Социальных Атрибутов*

Заклинатель издаёте боевой клич и/или короткую речь,
воодушевляющую его самого и/или других.
Цель чар становится невосприимчива к страху,
и будет продолжает сражаться даже тогда,
когда, казалось бы, надежды уже нет.

#### Примеры
 - **Паладин:** Воодушевляет личным примером
 - **Комиссар:** Расстреливает дезертиров
 - **Алхимик:** Даёт всем <<Озверин>>

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Пока эффект активен, цель заклинания обладает иммунитетом к *Запугиванию*
и *не может* бежать из сражения, даже если она этого хочет, этого требуют обстоятельства,
а командующим офицером был дан соответствующий приказ.

Для успешного применения на НИП (даже союзного),
необходимо совершить магический бросок против его уровня.
Время действия определяется результатом броска.

Уровни заклинания:
 - **Уровень 1 -- <<Упорство>>:**  
   Заклинатель воодушевляет только себя
 - **Уровень 2 -- <<Эй, ты!>>:**  
   Заклинатель способен воодушевить другого персонажа
 - **Уровень 3:**  
    * *Эффект <<Эй, вы>>:*  
      Заклинатель может воодушевить несколько целей,
      количество целей определяется показателем Шкалы
    * *Эффект <<Капеллан>>:*  
      Альтернативно, это заклинание можно применить на группы союзников или противников.
      Такая группа не будет бежать даже при отсутствии (или после гибели) лидера

---
<!-- Spell: Confuse -->
## Замешательство / Оглушение
*Похожего эффекта можно добиться, если бросить в глаза противника песком, оглушить светошумовой гранатой
или удачно совершить проверку Физических Атрибутов на атаку по частям тела*

Колдун как-либо лишает противника концентрации, попутно оглушая его.
Эффективно это понижает его боевой потенциал, мешая либо защищаться, либо атаковать.

#### Примеры
 - **Повелитель Зверей:** натравливает на жертву рой насекомых которые лезут в глаза
 - **Люминатор:** Частично ослепляет противника яркой вспышкой света
 - **Варвар:** Оглушает противника боевым кличем

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Для успешного применения заклинания требуется совершить магический бросок против уровня цели.
Каждый эффективный успех снижает Атаку или Защиту цели -- эффект необходимо выбрать заранее.
Тупые или <<хрупкие>> создания особенно уязвимы.

Уровни заклинания:
 - **Уровень 1 -- <<Касание>>:**  
   Колдун способен обратить в замешательство цель только на расстоянии касания
 - **Уровень 2 -- <<Взгляд>>:**  
   Колдун способен обратить в замешательство цель в пределах видимости
 - **Уровень 3:**  
    * *Эффект <<Несколько>>:*  
      Колдун способен обратить в замешательство несколько одиночных целей в пределах видимости,
      количество целей определяется показателем Шкалы
    * *Эффект <<Куча-мала>>:*  
      Альтернативно, заклинание можно применять против групповых противников --
      в таком случае оно наносит ущерб их численности,
      равный количеству *эффективных* успехов в тесте против их уровня
      (эквивалентно атаке *массовым* оружием)

---
<!-- Spell: Fear -->
## Испуг / Омерзение
*Похожего эффекта можно добиться, удачно совершив проверку Социальных Атрибутов*

Колдун как либо пугает и/или вызывает отвращение цели,
и она пытается оказаться как можно дальше от источника страха/омерзения.
Попавший под воздействие будет пытаться всячески избегать источника страха.

#### Примеры
 - **Воин:** Пугает жертву своей решимостью
 - **Стрелок:** Демонстративно целиться в лоб мишени
 - **Маг:** Демонстрирует свою мощь

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Необходимо совершить магический бросок против уровня цели.
Каждый эффективный успех уменьшает Храбрость цели.
*(Когда Храбрость противника опускается до 0, он начинает бежать.)*

Трусливые создания особенно уязвимы.

Уровни заклинания:
 - **Уровень 1 -- <<Касание>>:**  
   Колдун способен испугать цель только на расстоянии касания
 - **Уровень 2 -- <<Взгляд>>:**  
   Колдун способен испугать цель в пределах видимости
 - **Уровень 3:**  
    * *Эффект <<Несколько>>:*  
      Колдун способен испугать несколько одиночных целей в пределах видимости,
      количество целей определяется показателем Шкалы
    * *Эффект <<Аура ужаса>>:*  
      Альтернативно, заклинание можно применять против групповых противников --
      в таком случае оно наносит ущерб их численности,
      равный количеству *эффективных* успехов в тесте против их уровня
      (эквивалентно атаке *массовым* оружием)

---
<!-- Spell: Magic Resistance -->
## Магическое сопротивление
*Похожего эффекта можно добиться, удачно совершив проверку Физических Атрибутов*

Чародей окружает цель магическим щитом, который частично или полностью поглощает магический урон.

#### Примеры
 - **Монах:** Поглощает урон за счёт силы своей воли
 - **Маг:** Поднимает чародейский щит
 - **Телепат:** Применяет контрзаклинание

#### Эффект
*Тип: [Реакция], [Мгновенное]*

Когда допустимый союзник подвергается магической атаке любого рода,
заклинатель может активировать эту реакцию, чтобы повысить магическое сопротивления цели.
Каждый успех на магическом броске считается защитой персонажа на воздействие,
т.е. снижает число эффективных успехов.

Уровни заклинания:
 - **Уровень 1 -- <<На себя>>:**
   Маг может защитить только себя
 - **Уровень 2 -- <<На союзника>>:**
   Маг может защитить другого персонажа
 - **Уровень 3 -- <<Неуязвимая толпа>>:**
   Маг может защитить нескольких персонажей единовременно.
   Количество целей определяется показателем Шкалы

---
<!-- Spell: Tracker -->
## Метка
Маг ставит на цель (персонажа, вещь или местность) мистическую метку.
Он может понимать, где находится помеченная цель, независимо от расстояния до неё --
точность эффекта зависит от уровня заклинания.

#### Примеры
 - **Тауматург:** Капает каплю своей крови
 - **Рунист:** Рисует руну
 - **Варвар:** Вдыхает запах и запоминает его

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Заклинатель накладывает на цель особую метку.
Время работы метки определяется результатом магического броска в часах или днях.
(Если цель сопротивляется либо метку необходимо поставить скрытно, то бросок совершается против уровня цели.)
Пока заклинание активно, чародей может точно отслеживать помеченного в пределах региона, определяемого уровнем заклинания.
Если он покидает эту зону, точность слежения значительно снижается (пропорционально удалению).

Если у персонажа нет активных меток (либо не превышен лимит),
то он может задним числом поставить её на своего сопартийца,
спутника или личный предмет с помощью механики [Флешбеков][флешбек].

Уровни заклинания:
 - **Уровень 1 -- <<Касание>>:**  
   Маг может поставить метку, коснувшись цели.
   Зона точного обнаружения ограничена текущей локацией (сцена, дом, улица)
 - **Уровень 2 -- <<Взгляд>>:**  
   Маг может поставить метку на расстоянии взгляда.
   Зона точного обнаружения ограничена текущим районом
 - **Уровень 3 -- <<Миникарта>>:**  
   Маг может держать несколько активных меток.
   Их количество определяется показателем Шкалы.
   Зона точного обнаружения ограничена текущей зоной (город, лес)

---
<!-- Spell: Telepathy -->
## Мыслесвязь / Чужой взор
*Необходимо выбрать спектр существ, с которыми маг может устанавливать контакт, например:
гуманоиды, животные, мёртвые, демоны, духи природы*

Маг создаёт телепатическую связь между двумя целями.
<<Связанные>> могут передавать друг другу послания,
в том числе сложные концепции и планы,
демонстрировать то, что видят слышат, чуют или ощущают сами.
<<Связанные>> понимают друг друга, даже если не знают языка.

#### Примеры
 - **Пиромант:** Устанавливает мыслесвязь через ожог
 - **Жрец:** Устанавливает связь через символ веры
 - **Телепат:** Может устанавливать мыслесвязь, посмотрев в глаза

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Создаёт канал для обмена информацией между двумя (или более*) целями.
Продолжительность сеанса зависит от результата броска,
но заклинатель может прервать его в любой момент.
После установления канала его поддержание на том же расстоянии не является обязательным (можете удаляться/расходиться).
С уровнем увеличивается дальность установления канала и количество других персонажей в нём.

Уровни заклинания:
 - **Уровень 1 -- <<Касание>>:**  
   Маг может установить связь с кем-либо, коснувшись его
 - **Уровень 2 -- <<Взгляд>>:**  
   Маг может установить мыслесвязь собой и другим персонажем в пределах восприятия,
   либо между двумя такими персонажами (не включая себя в канал)
 - **Уровень 3 -- <<Сеть>>:**  
   Маг может установить мыслесвязь с несколькими хорошо знакомыми целями, где бы те ни находились.
   Количество целей определяется показателем Шкалы

---
<!-- Spell: Spider Legs -->
## Паучьи лапы
*Похожего эффекта можно добиться, удачно совершив проверку Ловкость+Телосложение+Снаряжение*

Чародей можете даровать цели способность перемещаться по отвесным поверхностям и даже по потолку.

#### Примеры
 - **Геомант:** Ступает по отвесным скалам погружая ноги в камень как в песок
 - **Тауматург:** Источает кровь из рук и ног которая мгновенно сворачивается
 - **Оборотень:** Цепляться за поверхность когтями

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Пока заклинание активно, цель может свободно перемещаться
по любым твёрдым поверхностям, способным выдержать её вес.
Время действия зависит от результата броска.

Уровни заклинания:
 - **Уровень 1 -- <<На себя>>:**  
   Чародей способен наложить эффект только на себя
 - **Уровень 2 -- <<Взлетай>>:**  
   Чародей способен даровать способность другому персонажу
 - **Уровень 3 -- <<Поползли>>:**  
   Чародей может наложить эффект на несколько целей,
   количество целей определяется показателем Шкалы

---
<!-- Spell: Turn On/Off -->
## Починка / Поломка / Вкл-Выкл
*Необходимо выбрать тип предметов, которыми заклинатель повелевает.
Похожего эффекта можно добиться, удачно совершив проверку соответствующих Атрибутов*

Заклинатель может отладить, расстроить, включить или выключить что-либо за мгновение ока,
при условии, что объект не выведен из строя окончательно.

#### Примеры
 - **Алхимик:** Способен дистиллировать загрязнённые реактивы
 - **Повар:** Способен вернуть товарный вид испорченным продуктам
 - **Механик:** Потрясти механическую приблуду, и та заработает

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Для успешного воздействия на объект необходимо совершить магический бросок
против уровня этого объекта и/или сложности его поломки.

Уровни заклинания:
 - **Уровень 1 -- <<Очумелые ручки>>:**  
   Заклинатель способен починить или сломать что-то маленькое, повертев это в руках.
   Размер объекта должен быть таким, чтобы им было легко манипулировать одной рукой
 - **Уровень 2 -- <<Взгляд>>:**  
   Заклинатель способен починить что-то, посмотрев на объект.
   Объект должен быть такого размера и массы,
   чтобы один человек мог его перенести с места на место
 - **Уровень 3 -- <<Стройбригада>>:**  
   Заклинатель может починить что-то размером с грузовик или автобус

---
<!-- Spell: Taunt -->
## Привлечение
*Похожего эффекта можно добиться, удачно совершив проверку Социальных Атрибутов*

Маг как-либо привлекает внимание цели,
и она пытается оказаться, как можно ближе к заклинателю.
Она будет пытаться каким-либо образом вступить с чародеем в контакт,
будь то словесный или вооружённый -- в зависимости от обстановки.

#### Примеры
 - **Шут:** Показывает фокус
 - **Маг:** Выглядит необычно
 - **Воин:** Выглядит как достойный противник

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

В случае успешного применения цель будет стараться
сократить дистанцию между ней и источником интереса.
В мирной ситуации привлечение заставит её начать
с заклинателем взаимодействие в самой свойственной ей манере.
В экстремальной ситуации заставит противника атаковать мага.

Время действия определяется результатом броска.
Эффект спадёт, если с целью как либо взаимодействовать.

Уровни заклинания:
 - **Уровень 1 -- <<Эй, ты!>>:**  
   Маг способен спровоцировать цель только на расстоянии касания,
   и она будет следовать за заклинателем
 - **Уровень 2 -- <<Да, ты!>>:**  
   Маг способен спровоцировать цель в пределах видимости
 - **Уровень 3 -- <<Эй, вы все!>>:**  
   Маг способен спровоцировать несколько целей в пределах видимости,
   количество целей определяется показателем Шкалы.
   В случае *критического успеха* способен нанести урон групповым противникам,
   равный количеству эффективных успехов (не игнорирует сопротивление).
   А ещё после такого ему *однозначно* потребуется лечение.

---
<!-- Spell: Root -->
## Путы
*Похожего эффекта можно добиться, опутав противника либо ударив его по ногам*

Колдун как-либо опутывает противника, и лишает его подвижности.

#### Примеры
 - **Некромант:** Вызывает костяные руки из земли, и те удерживают жертву
 - **Геомант:** Превращает землю под ногами цели в зыбучий песок
 - **Маг Металла:** Увеличивает вес снаряжения цели

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Пока заклинание активно, цель не может никак перемещаться,
но может свободно атаковать или защищаться.

Время действия определяется результатом броска.
Неуклюжие создания особенно уязвимы.

Уровни заклинания:
 - **Уровень 1 -- <<Касание>>:**  
   Колдун способен опутать цель только на расстоянии касания
 - **Уровень 2 -- <<Взгляд>>:**  
   Колдун способен опутать цель в пределах видимости
 - **Уровень 3:**  
    * *Эффект <<Несколько>>:*  
      Колдун способен опутать несколько одиночных целей в пределах видимости,
      количество целей определяется показателем Шкалы
    * *Эффект <<Болото>>:*  
      Альтернативно, заклинание можно применять против групповых противников --
      в таком случае оно наносит ущерб их численности,
      равный количеству *эффективных* успехов в тесте против их уровня
      (эквивалентно атаке *массовым* оружием)

---
<!-- Spell: Alarm -->
## Сигнализация
*Необходимо выбрать тип объектов, на которые маг может поставить сигнализацию, например:
живые существа, какой-то тип вещей, слова*

Чародей может ставить сигнализацию на предметы,
живых, существ, зоны, слова и понятия
и получать сигналы когда что-либо совершают с ними.

#### Примеры
 - **Книжник:** Ставит сигнализацию на какой либо редкий термин
 - **Пастух:** Ставит сигнализацию на своих подопечных
 - **Друид:** Ставит сигнализацию на деревья

#### Эффект
*Тип: [Быстрое] или [Долгое], [Временное]*

Заклинатель ставит метку на объект, и получаете немедленное уведомление,
когда кто-либо что-либо совершает с этим объектом.
Информации о положении объекта это не даёт,
но может дать информацию о том, что его двигают.

Время работы метки определяется результатом магического броска в часах или днях.
С уровнем увеличивается зона, на которую можно удалиться от <<защищённой>> цели,
прежде чем с ней пропадёт связь.
Если маг покинет зону, но потом вернётесь в неё, то узнаете,
что происходило с объектом в момент вашего отсутствия.

Если у персонажа нет активных сигнализаций (либо не превышен лимит),
то он может задним числом поставить её с помощью механики [Флешбеков][флешбек].

> **Табу.**
> В качестве допустимой категории объектов для присмотра можно выбрать *слова*.
> В таком случае при применении заклинатель выбирает слово, и на него накладывается табу --
> он получает уведомление всякий раз, когда это слово произносится.
> Для особо редких слов (либо имён, что нельзя говорить)
> уведомление срабатывает даже от одной мысли об объекте.

Уровни заклинания:
 - **Уровень 1 -- <<Касание>>:**  
   Маг может поставить сигнализацию, коснувшись цели.
   Зона уведомления текущей локацией (сцена, дом, улица)
 - **Уровень 2 -- <<Взгляд>>:**  
   Маг может поставить сигнализацию на расстоянии взгляда.
   Зона уведомления текущим районом
 - **Уровень 3 -- <<Охранная корпорация>>:**  
   Маг может держать несколько активных сигнализаций.
   Их количество определяется показателем Шкалы.
   Зона уведомления ограничена текущей зоной (город, лес)

---
<!-- Spell: Lesser Healing -->
## Слабое лечение
*Похожего эффекта можно добиться, успешно пройдя проверку Атрибута Медицина*

Целитель способен исцелять лёгкие раны по щелчку пальцев,
но тяжёлые раны излечить он не способен.

#### Примеры
 - **Шаман:** Призывает всех духов без разбору
 - **Жрец:** Молится не специализирующемуся на исцелении богу
 - **Лекарь:** Буквально закидывает пациента медикаментами

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Для успешного исцеления необходимо совершить магический бросок против ран, полученных ранее целью.
При успешном применении цель восстанавливает себе одну рану за каждый эффективный успех.

Такое лечение может стабилизировать находящегося при смерти персонажа,
но не в состоянии мгновенно вывести его из комы.

> **Медленное лечение:**
> В случае [Долгой][долгое] активации это заклинание
> воздействует на всех союзников неподалёку,
> независимо от уровня заклинания.
> На третьем уровне такое исцеление имеет нулевую сложность
> (игнорирует ранее полученные раны).

Уровни заклинания:
 - **Уровень 1 -- <<Возложение рук>>:**  
   Целитель может лечить себя или союзника на дистианции касания
 - **Уровень 2 -- <<Взгляд>>:**  
   Целитель может лечить на расстоянии взгляда
 - **Уровень 3 -- <<Госпиталь>>:**  
   Целитель лечит несколько пациентов, находящихся в пределах видимости, разом.
   Количество пациентов определяется показателем Шкалы

---
<!-- Spell: Pull / Push -->
## Толчок / Магнит
*Похожего эффекта можно добиться успешной проверкой Атлелики (Ловкость + Телосложение)*

Колдун меняет расстояние между собой и целью.
Способы возможны разные -- волочить по земле, подбросить, оттолкнуть, пнуть...

#### Примеры
 - **Псайкер:** Отбрасывает от себя предмет силой мысли
 - **Щитоносец:** Отталкивает что-либо от себя
 - **Бард:** Отбрасывает объект, ударив по струнам

#### Эффект
*Тип: [Быстрое], [Мгновенное]*

Для успешного применения необходимо совершить магический бросок против уровня *противника*,
независимо от того, толкает заклинатель его или себя --
это отображает то, что противник будет пытаться разорвать/сократить дистанцию.
В случае успеха сдвигает цель на одну единицу дистанции (ближняя/дальняя/<<убежал>>),
либо двигает цель в пределах этой же зоны.
Если *противник* не сопротивляется, то смещение считается успешным в любом случае,
но в случае успешного прохождения теста можно получить дополнительное действие в этом раунде.

Хрупкие предметы могут портиться.

Уровни заклинания:
 - **Уровень 1:**  
   Необходимо выбрать специализацию:
    * *Эффект <<Прочь!>>:*  
      Маг отталкивает объект прочь от себя, либо себя от него
    * *Эффект <<Иди сюда!>>:*  
      Маг притягивает объект к себе, либо себя к нему
 - **Уровень 2 -- <<Волокуша>>:**  
   Маг толкает объект (или себя) в любом направлении
 - **Уровень 3:**  
    * *Эффект <<Чехарда>>:*  
      Маг выбирает точку в зоне видимости и стягивает или отталкивает несколько целей к ней или от неё,
      количество целей определяется показателем Шкалы
    * *Эффект <<Силовая волна>>:*  
      Альтернативно, заклинание можно применять против групповых противников --
      в таком случае оно наносит ущерб их численности,
      равный количеству *эффективных* успехов в тесте против их уровня
      (эквивалентно атаке *массовым* оружием)

---
<!-- Spell: Waterwalk -->
## Хождение по воде
*Похожего эффекта можно добиться обычным плаванием*

Позволяет ходить по воде с обычной скоростью,
словно это была бы обычная суша.

#### Примеры
 - **Жрец:** Ходит по воде как святов
 - **Рыцарь Смерти:** Под ногами рыцаря смерти вода превращается в лёд
 - **Гидромант:** Управляет водой, и она не поглощает его

#### Эффект
*Тип: [Быстрое] или [Долгое], [Мгновенное]*

Заклинатель может даровать цели способность ходить по воде некоторое время.
Это не дарует особой подвижности, не защищает от захлёбывания (если цель топят)
и не выталкивает на поверхность (если цель тонет).

Уровни заклинания:
 - **Уровень 1 -- <<Иисус>>:**  
   Ходить по воде способен только сам маг
 - **Уровень 2 -- <<Иисус 2.0>>:**  
   Маг может даровать способность ходить по воде другому
 - **Уровень 3 -- <<Моисей>>:**  
   Маг может даровать способность ходит по воде нескольким целям,
   количество целей определяется показателем Шкалы

<style type="text/css">
.col-md-9 hr { background-image: url('../img/splitters/splitter-3.png'); }
</style>

<!-- Общие магические термины -->
[быстрое]: ./spells.md#быстрое
[временное]: ./spells.md#временное
[долгое]: ./spells.md#долгое
[мгновенное]: ./spells.md#мгновенное
[перманентное]: ./spells.md#перманентное
[реакция]: ./spells.md#реакция

<!-- Прочие термины -->
[оглушение]: ../game-mechanics/attacks.md#оглушение
[прикрепить-масть]: ./spells.md#прикрепить-масть
[флешбек]: ../game-mechanics/personality.md#флешбеки
