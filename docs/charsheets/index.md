# Листы персонажей
## Лист персонажа v2.0
*Добавлен 2021-03-21*

![character-sheet-v2.0.jpg](./runeterra-character-sheet-v2.0.jpg "Лист персонажа v2.0")

---
## Лист персонажа v1.7
*Добавлен 2021-02-14*

![character-sheet-v1.7.jpg](./runeterra-character-sheet-v1.7.jpg "Лист персонажа v1.7")

---
## Лист персонажа v1.6
*Добавлен 2020-12-26*

![character-sheet-v1.6.jpg](./runeterra-character-sheet-v1.6.jpg "Лист персонажа v1.6")

---
## Лист персонажа v1.4
*Добавлен 2020-11-11*

![character-sheet-v1.4.jpg](./runeterra-character-sheet-v1.4.jpg "Лист персонажа v1.4")

---
## Лист персонажа v1.3
*Добавлен 2020-10-06*

![runeterra-character-sheet-v1.3.jpg](./runeterra-character-sheet-v1.3.jpg "Лист персонажа v1.3")

---
## Лист персонажа v1.0
*Добавлен ХЗ когда*

![runeterra-character-sheet-v1.0.jpg](./runeterra-character-sheet-v1.0.jpg "Лист персонажа v1.0")
