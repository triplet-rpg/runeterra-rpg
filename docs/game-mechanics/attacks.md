# Вариации атак и защиты от них
Во все атаки по умолчанию к успехам добавляется счетчик раундов.
Он начинается с 0, и увеличивается на 1 в конце каждого раунда.

## Обычные ближние атаки
Все атаки совершаются через навык Нападение.
Каждый успех сверху защиты оппонента наносит ему 1 урона.
Критичские успехи игнорирубт Защиту противника.
 - **Атака одноручным клинковым оружием:** Совершается через бросок *Ловкость + Нападение + Оружие*
 - **Атака двуручным клинковым оружием:** Совершается через бросок *Телосложение + Нападение + Оружие*
 - **Атака древковым/цепным/экзотическим оружием:** Совершается через бросок *Ловкость + Нападение + Оружие*
 - **Атака тяжелым/дробящим оружием:** Совершается через бросок *Телосложение + Нападение + Оружие*
 - **Атака дальнобойним оружием вблизи:** Совершается через бросок *Меткость + Нападение + Оружие*
 - **Атака из скрытности:** Совершается через бросок *Скрытность + Нападение + Оружие*
   Возможна только если игрок находится в *Скрытности* и использует скрытное оружие.
   Игнорирует Защиту. Критический успех наносит удвоенный урон

## Обычные дальние атаки
Все атаки совершаются через навык Меткость.
Каждый успех сверху защиты оппонента наносит ему 1 урона.
Критические успех игнорируют Защиту противника.
 - **Атака из огнестрела/арбалета:** Совершается через бросок *Ловкость + Меткость + Оружие*
 - **Метание оружия ближнего боя:** Совершается через бросок *Нападение + Меткость + Оружие*
 - **Метание гранат или атака из лука/пращи:** Совершается через бросок *Телосложение + Меткость + Оружие*.
   Чаще всего является массовым, все неуспехи являются атакой по союзникам в зоне поражения
 - **Огонь на подавление:** Совершается через бросок *Защита + Меткость + Оружие*.
   Не наносит урона, но уменьшает урон, входящий по союзникам
 - **Атака из скрытности:** Совершается через бросок *Скрытность + Меткость + Оружие*.
   Возможна только если игрок находится в *Скрытности* вдали от оппонента. Игнорирует Защиту. Крит-успех наносит удвоенный урон

## Атаки по частям тела
Совершая атаку игрок может заявить, что атакует конкретную часть тела оппонента.
Можно применять только против одиночных целей.
Урон в данном случае будет нанесен только в том случае,
если у персонажа соответствующий навык является {+бонусным+}.
Если противник является *Чудовищем*, необходим навык {+Медицина+}.
 - **Атака по оружию/рукам:** Все успехи снижают атаку противника до конца боя. Нельзя уменьшить ниже 0
 - **Атака по доспехам/ногам:** Все успехи снижают защиту противника до конца боя. Нельзя уменьшить ниже 0
 - **Атака в уязвимую точку (например, голову):** Можно проводить только если вражеская уязвимая точка обнаружена.
   Урон удвоен. Скрытная атака убивает цель *тихо*, если урона больше, чем ее уровень

## Социальные действия в бою
Различные действия могут считаться совместными.
Тогда выбирается максимум из двух бросков.
Если первый является критическим успехом, все успехи на вторую заявку так же будут критическим успехом.
Некоторые из них можно совершать в два разных действия как совместный бросок сам с собой.
 - **Финт:** Совершается через бросок *Обман + Провокация + Порядочность*.
   Является обычной атакой
 - **Воодушевление:** Совершается через бросок *Лидерство + Провокация*.
   Является совместным броском. Работает только на персонажей, которые считают вас своим Лидером
 - **Уязвимая точка:** Совершается через бросок *Внешность + Проницательность* или *Внимательность + Медицина*.
   Является совместным броском. Позволяет определить уязвимую точку противника
 - **Собственный пример:** Совершается через бросок *Внешность + Лидерство**.
   Возможно только если персонаж в ближнем бою. Является совместным действием
 - **Запугивание:** Совершается через бросок *Запугивание + Внешность + Грозность*.
   Понижает Храбрость одиночных противников. Против групповых противников является обычной атакой
 - **<<Я тут главный!>>:** Совершается через бросок *Лидерство + Обман + Порядочность*.
   В случае успеха все противники будут атаковать вас. Сварм в такой ситуации атакует с удвоенным уроном, но только вас
 - **<<Ни шагу назад!>>:** Совершается через бросок *Лидерство + Запугивание + Грозность*.
   Запуганные или оглушенные союзники снова вступают в бой. *Они потом вам вспомнят!*
 - **Поиск лидера:** Совершается через бросок *Проницательность + Лидерство*.
   Находит вражеского лидера. Если его убить, противники отступят

## Вариации защиты
От разных атак игроки защищаются по разным броскам. Обычно монстр атакует на *уровень* урона. Атак по частям тела монстр не проводит. Исключение - дуэль 1х1.
При крит-успехе игрок может совершить свободное действие с количеством успехов равным *крит-успеху*.
 - Защита щитом: Совершается по броску **Защита + Телосложение**. Можно применять только если есть щит, можно использовать вместо любых атак, где защита щитом применима. Не работает против магии.
 - Защита в ближнем бою: Совершается по броску **Защита + Нападение**. Если есть {+Защита+}, можно добавить оружие ближнего боя в преимущества. Оружие в бонус не работает против свармов. Если есть {-Нападение-}, урон, получаемый игроком, удвоен.
 - Защита в дальнем бою: Совершается по броску **Защита + Меткость**. Если есть *укрытие*, его уровень работает как преимущество. Если есть {-Меткость-}, урон, получаемый игроком, удвоен.
 - Защита от магии, падений или АоЕ: Совершается по броску **Защита + Ловкость**. Если есть {-Защита-}, урон, получаемый игроком, удвоен.
 - Защита от кражи: Совершается по броску **Защита + Скрытность**: Тяжелое оружие украсть не могут, каждый успех монстра против игрока крадет 1 точку *вещей* - богатство, артефакты, мелкое оружие, снаряжение или расходники в таком порядке.
 - Защита от химии или оглушающих атак: Совершается по броску **Защита + Телосложение**. Этот же бросок совершается при {-Телосложении-} на то, чтобы прожить несколько раундов.
