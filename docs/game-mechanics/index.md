# Основные механики
 - [Система Воли](./personality.md)
 - [Атрибуты и связанные с ними броски](./attributes-and-rolls.md)
 - [<<Выделенные>> атрибуты](./highlighted-attributes.md)
 - [Социальные преимущества](./social-benefits.md)
 - [Боевые действия](./attacks.md)
 - [Снаряжение](./equipment.md)
