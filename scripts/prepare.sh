#!/usr/bin/env bash

cd $(dirname $0)/..
source scripts/env.sh

set -eu
set -v

function usage()
{
    cat << EOF
Prepare script usage:
$0 [OPTIONS]

Executes pre-build setup scripts.
Optionally removes the compiled files.
Optionally configures the CI repository.

Available options:
  --setup, -s   | Run initial setup
EOF
}

CLEAN_OPT=false
SETUP_OPT=false
FAST_OPT=false

for ARG in "$@"
do
    case "${ARG}" in
        --clean|-c)
            CLEAN_OPT=true
            ;;

        --setup|-s)
            SETUP_OPT=true
            ;;

        --fast|-f)
            FAST_OPT=true
            ;;

        *)
            echo "Unsupported option: '${ARG}'" >&2
            usage
            exit 1
    esac
done

if [[ ${CLEAN_OPT} == true ]]
then
    rm -rvf public/
    rm -rvf mkdocs.yml
fi

if [[ ${SETUP_OPT} == true ]]
then
    export CI_SCRIPTS_REPO_URL="${CI_REPOSITORY_URL%"${CI_PROJECT_PATH}.git"}${CI_SCRIPTS_REPO}.git"
    mkdir -p "$(dirname "${SCRIPTS_DIRECTORY}")"
    pushd "$(dirname "${SCRIPTS_DIRECTORY}")"
        git init
        git remote add origin "${CI_SCRIPTS_REPO_URL}"
        git fetch origin
        git checkout -b master --track origin/master # origin/master is clone's default
    popd

    bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/python/util_create-virtual-env.sh"
fi

if [[ ${FAST_OPT} != true ]]
then
    source "${VIRTUALENV_BIN}/activate"
    bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/mkdocs/doc_pre-install-deps.sh"
    bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/mkdocs/doc_pre-install-markdown-extensions.sh"
fi
