export BASH_OPTIONS='-x'
export CI_COMMIT_REF_NAME=$(git branch --no-color | grep -- '*' | grep --color=never -Po '[^\s\*]+')
export CI_PROJECT_DIR=$(realpath .)
export CI_PROJECT_PATH='triplet-rpg/runeterra-rpg'
export CI_PROJECT_TITLE='Runeterra RPG'
export CI_PROJECT_URL='https://gitlab.com/triplet-rpg/runeterra-rpg'
export CI_REPOSITORY_URL='https://gitlab.com/triplet-rpg/runeterra-rpg.git'
export CI_PAGES_URL='https://triplet-rpg.gitlab.io/runeterra-rpg'
export GITLAB_USER_EMAIL='ussx-hares@yandex.ru'

export WORKSPACE=$(realpath .)
export SCRIPTS_DIRECTORY="${WORKSPACE}/.gitlab-ci/scripts"
# export SCRIPTS_DIRECTORY=$(realpath ../../tools/gitlab-ci-mirror/scripts)
export CI_SCRIPTS_REPO='hares-lab/gitlab-ci'

export CSS_REL_PATH='css'
export DOCS_REL_PATH='docs'
export DOCS_DIR="${CI_PROJECT_DIR}/${DOCS_REL_PATH}"
export MKDOCS_CONF_NAME="${CI_PROJECT_DIR}/mkdocs.yml"
export MKDOCS_NAV_CONF_NAME="${CI_PROJECT_DIR}/mkdocs.part.yml"
export README_NAME='README.md'

OS_TYPE=
VIRTUALENV_BIN=
case "$(uname -s)" in
    Linux*)
        OS_TYPE='Linux'
        VIRTUALENV_BIN='.venv/bin'
        ;;
    Darwin*)
        OS_TYPE='Mac'
        VIRTUALENV_BIN='.venv/bin'
        ;;
    CYGWIN*)
        OS_TYPE='Cygwin'
        VIRTUALENV_BIN='.venv/Scripts'
        ;;
    MINGW*)
        OS_TYPE='MinGw'
        VIRTUALENV_BIN='.venv/Scripts'
        ;;
    *)
        OS_TYPE="UNKNOWN:$(uname -s)"
        VIRTUALENV_BIN='.venv/bin'
        ;;
esac
