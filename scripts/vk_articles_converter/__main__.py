
# from vk_articles_converter.api import convert_articles
from vk_articles_converter.core import ConverterManager
from vk_articles_converter.config import *
from vk_articles_converter.postprocessor import MagicArticlePostProcessor

def main():
    converter = ConverterManager(article_converter_class=MagicArticlePostProcessor, group_id=GROUP_ID, input_directory=HTML_SOURCES_DIR, output_directory=OUTPUT_DIR, vk_domain=VK_DOMAIN)
    for page, converted_name in PAGES_MAPPING.items():
        converter.register_article(page, converted_name)
    
    converter.create_index()
    converter.dump_index()
    converter.process_all_articles()
    
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
