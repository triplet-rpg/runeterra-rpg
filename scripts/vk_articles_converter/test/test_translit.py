import unittest
from vk_articles_converter.translit import transliterate_name

class MyTestCase(unittest.TestCase):
    def test_translit(self):
        data = "Сверхъестественные Воздействия I Уровня"
        expected = "sverhestestvennye-vozdeistviya-i-urovnya"
        actual = transliterate_name(data)
        
        self.assertEqual(expected, actual)

if (__name__ == '__main__'):
    unittest.main()
