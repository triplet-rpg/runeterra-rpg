import re
from bs4.element import Tag
from functional.option import *

from .core import VKArticleConverter

class MagicArticlePostProcessor(VKArticleConverter):
    _last_myst_level: Option[str] = OptionNone
    
    def convert_strong(self, el: Tag, text: str) -> str:
        if (re.fullmatch(r'\**', text)):
            return ''
        elif (text == "Действие Эффекта"):
            return '#### ' + text
        else:
            return super().convert_strong(el, text)
    
    def convert_p(self, el: Tag, text: str) -> str:
        pat = re.compile(r'^(?:\s*\**)*\s*МИСТ\s*\.?\s*(?P<level>(?P<level_num>\d+)(?P<level_additional> *\(\w+\))?)\s*\**\s*(?::|-+)\s*')
        m = pat.match(text)
        if (m):
            if (m.group('level_additional')):
                if (self._last_myst_level != Some(m.group('level_num'))):
                    text = pat.sub(r' - **Уровень \g<level_num>:**\n* ', text)
                    lines = text.splitlines(keepends=False)
                    lines[1] = ' '*4 + lines[1]
                    start_with = 2
                else:
                    text = pat.sub(r'* ', text)
                    lines = text.splitlines(keepends=False)
                    lines[0] = ' '*3 + lines[0]
                    start_with = 1
                
                for i in range(start_with, len(lines)):
                    lines[i] = ' '*6 + lines[i]
                self._last_myst_level = Some(m.group('level_num'))
            
            else:
                text = pat.sub(r' - **Уровень \g<level>:** ', text)
                lines = text.splitlines(keepends=False)
                for i in range(1, len(lines)):
                    lines[i] = ' '*4 + lines[i]
                self._last_myst_level = OptionNone
            
            text = '\n'.join(lines)
            text += '\n'
            el.name = 'li'
        
        else:
            text = super().convert_p(el, text)
        
        return text
    
    def convert_hn(self, n: int, el: Tag, text: str) -> str:
        if ((n == 3 or n == 5) and re.match(r'(Эффект|Действие|Действие [Ээ]ффекта|Эффект [Дд]ействия):?', text)):
            n = 4
            text = 'Эффект'
        elif (n == 3 and re.match(r'(Описание):?', text)):
            n = 4
            text = 'Описание'
        else:
            n -= 1
        
        return super().convert_hn(n, el, text)



__all__ = \
[
    'MagicArticlePostProcessor',
]
