import os
import re
from typing import *
from urllib.parse import parse_qs, urlencode

import six
from bs4 import BeautifulSoup
from bs4.element import Tag, NavigableString
from dataclasses import dataclass, field
from functional.option import *
from slugify_smart import slugify_smart
from markdownify import MarkdownConverter, ATX
from setuptools.glob import iglob

from .translit import transliterate_name

DEFAULT_VK_DOMAIN = 'https://vk.com'

class VKArticleConverter(MarkdownConverter):
    class DefaultOptions(MarkdownConverter.DefaultOptions):
        bullets: str = '-*+'
        heading_style: str = ATX
        long_line_length: int = 80
        very_long_line_length: int = 120
    
    class Options(DefaultOptions, MarkdownConverter.Options):
        pass
    
    name: str
    id: str
    manager: 'ConverterManager'
    def __init__(self, name: str, manager: 'ConverterManager', **options):
        self.manager = manager
        self.name = name
        self.id = self.manager.article_id(self.name)
        
        super().__init__(**options)
    
    def convert(self, html: str) -> str:
        return self.post_process(super().convert(html))
    
    def create_index(self, html: str):
        self.manager.articles_name_mapping.setdefault(self.id, self.name)
        self.manager.articles_headers_mapping.setdefault(self.id, dict())
        
        soup = BeautifulSoup(html, 'html.parser')
        self.process_tag(soup, children_only=True, dry_run=True)
    
    def post_process(self, markdown: str) -> str:
        result = markdown
        
        result = re.sub(r'(^ *\n)+', '\n', result, flags=re.MULTILINE)
        result = re.sub(r'\n*(^ *\*+ *\n)+\n*', '\n', result, flags=re.MULTILINE)
        
        return result
    
    def rewrite_special_symbols(self, text: str) -> str:
        text = re.sub(r'[”“"«]\b', r'<<', text)
        text = re.sub(r'(\S|$|^)[”"»]', r'\1>>', text)
        text = re.sub(r'\\', '/', text)
        text = re.sub(r'(\s)[\-—](\s)', r'\1--\2', text)
        
        return text
    
    def _wrap_long_line_iteration_iteration(self, line: str, special_symbols: List[Tuple[str, str]]) -> Option[Tuple[str, str]]:
        _len = len(line)
        _half = len(line) // 2
        
        for step in range(0, _half, 10):
            for pat, repl in special_symbols:
                pat = re.compile(pat)
                L = _half - step//2
                R = _half + step//2
                part = line[L:R]
                if (pat.search(part)):
                    new_line = pat.sub(repl, part, 1)
                    left, _, right = new_line.partition('\n')
                    
                    return Some((line[:L] + left, right + line[R:]))
        
        return OptionNone
    
    def _wrap_long_line_iteration(self, line: str) -> Tuple[str, str]:
        groups = \
        [
            [ (r': ', r':\n'), (r' +-- ', r' --\n') ],
            [ (r' \(', r'\n('), (r'\) ', r')\n') ],
            [ (r', ', r',\n') ],
            [ (' ', '\n') ]
        ]
        for symbol_group in groups:
            opt = self._wrap_long_line_iteration_iteration(line, symbol_group)
            if (opt): return opt.get
        else:
            raise Exception(f"Why are being you so difficult? Unable to wrap line '{line}'")
    
    def wrap_long_lines(self, text: str) -> str:
        text = re.sub(r'([.!?]+) +', r'\1\n', text)
        lines = text.splitlines(keepends=False)
        i = 0
        while (i < len(lines)):
            line = lines[i]
            if (len(line) > self.options['long_line_length']):
                left, right = self._wrap_long_line_iteration(line)
                lines[i] = left
                lines.insert(i+1, right)
                i -= 1
            
            i += 1
        
        text = '\n'.join(lines)
        return text
    
    def process_tag(self, node: Tag, *, children_only: bool = False, dry_run: bool = False):
        text = ''
        
        # Convert the children first
        for el in node.children:
            if (isinstance(el, NavigableString)):
                if (not dry_run):
                    t = self.process_text(six.text_type(el))
                    if (node.name == 'p'):
                        t = self.wrap_long_lines(t)
                    elif (self.is_header_tag(node.name)):
                        t = t.title()
                    text += t
            else:
                text += self.process_tag(el, dry_run=dry_run)
        
        if (not children_only and (not dry_run or self.is_header_tag(node.name))):
            convert_fn = getattr(self, 'convert_%s' % node.name, None)
            if (convert_fn and self.should_convert_tag(node.name)):
                text = convert_fn(node, text)
            if (self.should_reposition_linebreaks(node.name)):
                text = self.reposition_linebreaks(text)
        
        return text
    
    def process_text(self, text: str) -> str:
        return super().process_text(self.rewrite_special_symbols(text))
    
    def should_reposition_linebreaks(self, tag_name: str) -> bool:
        return self.is_header_tag(tag_name) or tag_name in { 'blockquote', 'p' }
    
    def is_header_tag(self, tag_name: str) -> bool:
        return bool(re.fullmatch(r'h\d', tag_name))
    
    def reposition_linebreaks(self, text: str):
        return '\n' + text.rstrip() + '\n'
    
    def convert_a(self, el: Tag, text: str) -> str:
        if ('href' in el.attrs):
            dest = el.attrs['href']
            if ('title' not in el.attrs or el.attrs['title'] == dest):
                el.attrs['title'] = ''
            
            parsed = self.manager.extract_article_ir_from_url(dest)
            if (parsed):
                article_id, _, query = parsed.get.partition('?')
                article_id, _, anchor = article_id.partition('#')
                anchor_pretty = ''
                
                if (article_id in self.manager.articles_headers_mapping):
                    parsed_query = parse_qs(query)
                    if ('anchor' in parsed_query):
                        anchor = parsed_query.pop('anchor').pop()
                        query = urlencode(parsed_query)
                    
                    if (anchor in self.manager.articles_headers_mapping[article_id]):
                        anchor_pretty = self.manager.articles_headers_mapping[article_id][anchor]
                        anchor = slugify_smart(anchor_pretty, '-')
                
                base_dest = self.manager.calculate_rel_path(self.id, article_id)
                dest = base_dest.get_or_else('')
                if (anchor): dest += '#' + anchor
                if (query): dest += '?' + query
                
                el.attrs['href'] = dest
                
                title = Option(el.attrs.pop('title', None) or None)
                if (not title):
                    title = base_dest.map(lambda _: self.manager.articles_name_mapping.get(article_id, article_id))
                    if (anchor_pretty): title = Some(title.map('{} /'.format).get_or_else('') + anchor_pretty)
                
                if (title):
                    el.attrs['title'] = title.get
        
        return super().convert_a(el, text)
    
    def _convert_header_id(self, el: Tag) -> str:
        if ('id' in el.attrs):
            return el.attrs['id']
        elif (el.contents):
            for child in el.contents:
                if (isinstance(child, Tag) and 'article_anchor_button' in child.attrs.get('class', '') and 'id' in child.attrs):
                    return child.attrs['id']
        
        return transliterate_name(el.text)
    
    def convert_hn(self, n: int, el: Tag, text: str) -> str:
        self.manager.articles_headers_mapping.get(self.id, dict()).setdefault(self._convert_header_id(el), el.text)
        return super().convert_hn(n, el, text)


@dataclass
class ConverterManager:
    input_directory: str
    output_directory: str
    group_id: Union[str, int]
    article_converter_class: Type[VKArticleConverter] = VKArticleConverter
    vk_domain: str = DEFAULT_VK_DOMAIN
    
    articles_name_mapping: Dict[str, str] = field(default_factory=dict)
    """ Maps article transliterated name (ID) to normal name (text) """
    
    articles_headers_mapping: Dict[str, Dict[str, str]] = field(default_factory=dict)
    """ Maps article transliterated name (ID) to map of header transliterated name (ID) to header normal name (text) """
    
    articles_conversion_mapping: Dict[str, str] = field(default_factory=dict)
    """ Maps article transliterated name (ID) to filename (with extension) """
    
    def __post_init__(self):
        self.input_directory = os.path.abspath(self.input_directory)
        self.output_directory = os.path.abspath(self.output_directory)
    
    @property
    def article_url_prefix(self) -> str:
        if (isinstance(self.group_id, int)):
            prefix = f'-{self.group_id}'
        else:
            prefix = self.group_id
        
        return f'{self.vk_domain}/@{prefix}-'
    
    @property
    def article_files(self) -> Iterator[str]:
        return iglob(os.path.join(self.input_directory, '*.html'))
    
    def article_converter(self, name: str) -> VKArticleConverter:
        return self.article_converter_class(name, self)
    
    def register_article(self, old_name: str, name: str):
        self.articles_conversion_mapping[self.article_id(old_name)] = name
    
    def add_article_to_index(self, name: str, text: str):
        self.article_converter(name).create_index(text)
        pass
    
    def process_article(self, filename: str, text: str) -> str:
        return self.article_converter(filename).convert(text)
    
    @classmethod
    def _load(cls, path: str) -> str:
        with open(path, 'rt', encoding='utf8') as f:
            return f.read()
    
    @classmethod
    def _save(cls, path: str, content: str):
        with open(path, 'wt', encoding='utf8') as f:
            f.write(content)
    
    @classmethod
    def article_name(cls, filename: str) -> str:
        name = os.path.basename(filename)
        if ('.' in name):
            name, _, _ = name.rpartition('.')
        
        return name

    @classmethod
    def article_id(cls, name: str) -> str:
        return transliterate_name(name)
    
    def convert_article_name(self, name: str) -> str:
        return self.convert_article_id(self.article_id(name))
    
    def convert_article_id(self, article_id: str) -> str:
        if (article_id in self.articles_conversion_mapping):
            article_id = self.articles_conversion_mapping[article_id]
        
        return article_id
    
    def construct_article_url(self, id: str) -> str:
        return self.article_url_prefix + id
    
    def extract_article_ir_from_url(self, url: str) -> Option[str]:
        left, sep, result = url.partition(self.article_url_prefix)
        
        if (sep and not left):
            return Some(result)
        else:
            return OptionNone
    
    
    def create_index(self):
        print("Creating index...")
        for p in self.article_files:
            self.add_article_to_index(self.article_name(p), self._load(p))
        print("Index created.")
    
    def dump_index(self):
        print("Index dump:")
        for article_id, article_headers in self.articles_headers_mapping.items():
            print('----')
            print(f"Article {article_id!r}:")
            for header_id, header_text in self.articles_headers_mapping[article_id].items():
                print(' '*4 + f"Header {header_id!r}: {header_text!r}")
            
            print()
    
    def process_all_articles(self):
        if (not os.path.isdir(self.output_directory)):
            os.makedirs(self.output_directory, mode=0o755)
        
        for p in self.article_files:
            name = self.article_name(p)
            print(f"Converting file '{name}'...")
            converted = self.process_article(name, self._load(p))
            self._save(os.path.join(self.output_directory, self.convert_article_name(name)), converted)
        
        print("Converted.")
    
    def calculate_rel_path(self, src_article_id: str, dest_article_id: str) -> Option[str]:
        src_converted_id = self.convert_article_id(src_article_id)
        dst_converted_id = self.convert_article_id(dest_article_id)
        
        if (src_converted_id == dst_converted_id):
            return OptionNone
        else:
            result = os.path.relpath(dst_converted_id, os.path.dirname(src_converted_id))
            if (not result.startswith('.')):
                result = './' + result
            return Some(result)


__all__ = \
[
    'DEFAULT_VK_DOMAIN',
    'VKArticleConverter',
    'ConverterManager',
]
