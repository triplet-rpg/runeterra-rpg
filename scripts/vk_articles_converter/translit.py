import re

from transliterate import translit
from transliterate.base import TranslitLanguagePack, registry
from transliterate.contrib.languages.ru.translit_language_pack import RussianLanguagePack

mapping = (
    u"abvgdeziklmnoprstufhcC'y'ABVGDEZIJKLMNOPRSTUFH'Y'",
    u"абвгдезиклмнопрстуфхцЦъыьАБВГДЕЗИЙКЛМНОПРСТУФХЪЫЬ",
)

reversed_specific_mapping = (
    u"йЙёэЁЭъьЪЬ",
    u"иИeeEE''''"
)

pre_processor_mapping = {
    u"zh": u"ж",
    u"ts": u"ц",
    u"ch": u"ч",
    u"sh": u"ш",
    u"sch": u"щ",
    u"yu": u"ю",
    u"ya": u"я",
    u"Zh": u"Ж",
    u"Ts": u"Ц",
    u"Ch": u"Ч",
    u"Sh": u"Ш",
    u"Sch": u"Щ",
    u"Yu": u"Ю",
    u"Ya": u"Я"
}

class AlternateRussianLanguagePack(TranslitLanguagePack):
    """Language pack for Russian language.

    See `http://en.wikipedia.org/wiki/Russian_alphabet` for details.
    """
    language_code = "ru"
    language_name = "Russian"
    character_ranges = ((0x0400, 0x04FF), (0x0500, 0x052F))
    mapping = mapping
    reversed_specific_mapping = reversed_specific_mapping
    pre_processor_mapping = pre_processor_mapping
    detectable = True

registry.unregister(RussianLanguagePack)
registry.register(AlternateRussianLanguagePack)

def transliterate_name(name: str, *, locale: str = 'ru') -> str:
    name = translit(name, locale, reversed=True)
    name = re.sub(r'[^\w\-\s]', '', name)
    name = re.sub(r'\W+', '-', name)
    
    return name.lower()

__all__ = \
[
    'AlternateRussianLanguagePack',
    'transliterate_name',
]


def main():
    actual = transliterate_name("Сверхъестественные Воздействия I Уровня")
    expected = "sverhestestvennye-vozdeistviya-i-urovnya"
    
    assert actual == expected
    
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
