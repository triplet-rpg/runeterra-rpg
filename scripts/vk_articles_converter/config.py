from .core import DEFAULT_VK_DOMAIN

WORKSPACE_DIR = 'docs'
HTML_SOURCES_DIR = WORKSPACE_DIR + '/magic/.temp/html'
OUTPUT_DIR = WORKSPACE_DIR

VK_DOMAIN = DEFAULT_VK_DOMAIN
GROUP_ID = 68886808

PAGES_MAPPING = \
{
    "Сверхъестественные Воздействия":            'magic/core.md',
    "Сверхъестественные Воздействия I Уровня":   'magic/spells-tier-1.md',
    "Сверхъестественные Воздействия II Уровня":  'magic/spells-tier-2.md',
    "Сверхъестественные Воздействия III Уровня": 'magic/spells-tier-3.md',
    "Сверхъестественные Воздействия IV Уровня":  'magic/spells-tier-4.md',
    "Сверхъестественные Воздействия V Уровня":   'magic/spells-tier-5.md',
    "Сверхъестественные Воздействия VI Уровня":  'magic/spells-tier-6.md',
    "Сверхъестественные Воздействия VII Уровня": 'magic/spells-tier-7.md',
}
